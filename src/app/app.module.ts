import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

// Modules
import { BallSelectorModule } from './containers/ball-selector/ball-selector.module';
import { BetSlipModule } from './containers/bet-slip/bet-slip.module';
import { ModalModule } from './components/modal/modal.module';

// Components
import { AppComponent } from './app.component';

// Services
import { ModalService } from './components/modal/modal.service';

@NgModule({
  declarations: [AppComponent],
  imports: [BrowserModule, BetSlipModule, BallSelectorModule, ModalModule],
  providers: [ModalService],
  bootstrap: [AppComponent],
})
export class AppModule {}
