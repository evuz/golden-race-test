import { Component, OnInit } from '@angular/core';
import { flatMap, take } from 'rxjs/operators';

import { GameService } from './services/game/game.service';
import { UserService } from './services/user/user.service';
import { ModalService } from './components/modal/modal.service';
import { resultText } from './utils/gameLiterals';

const RECHARGE = 200;
const INIT = 400;

@Component({
  selector: 'gr-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  private amount: number;
  private stake: number;
  constructor(private gameSrv: GameService, private userSrv: UserService, private modalSrv: ModalService) {}

  ngOnInit() {
    this.userSrv.amount.pipe(take(1)).subscribe(amount => {
      if (amount === null) {
        this.modalSrv.openModal({
          msg: `Welcome! As a reward, we are going to give you a credit of ${INIT}€.`,
        });
        this.userSrv.setAmount(INIT);
      }
    });
    this.userSrv.amount.subscribe(val => (this.amount = val));
    this.gameSrv.stake.subscribe(val => (this.stake = val));
  }

  play() {
    if (this.stake > this.amount) {
      return this.modalSrv.openModal({
        msg: 'Your amount must be greater than stake'
      })
    }
    this.gameSrv
      .play()
      .pipe(
        flatMap(({ profit, winner }) => {
          this.userSrv.updateAmount(profit);
          return this.modalSrv.openModal({
            msg: resultText(profit),
            ballNumber: winner,
          });
        }),
      )
      .subscribe(() => {
        if (this.amount <= 0) {
          this.modalSrv.openModal({
            msg: `You have lost all your credit, but we have recharged it with ${RECHARGE}€`,
          });
          this.userSrv.setAmount(RECHARGE);
        }
      });
  }
}
