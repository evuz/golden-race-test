import { BallColor } from "../components/balls/ball-color.model";

export const colors: {[e: number]: BallColor} = {
  0: { background: 'lightcoral', font: 'black' },
  1: { background: 'lightgreen', font: 'black' },
  2: { background: 'lightcyan', font: 'black' },
  3: { background: 'lightsalmon', font: 'black' },
  4: { background: 'lightpink', font: 'black' },
  5: { background: 'lightgoldenrodyellow', font: 'black' },
  6: { background: 'lightskyblue', font: 'black' },
  7: { background: 'lightslategray', font: 'black' },
  8: { background: 'darkorange', font: 'white' },
  9: { background: 'darkslategrey', font: 'white' },
  10: { background: 'darkgoldenrod', font: 'white' },
};
