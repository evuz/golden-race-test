import { FormControl } from '@angular/forms';

// Angular Validators.pattern doesn't work correct with regexp
export const patternValidator = (form: FormControl) => {
  const { value } = form;
  if (!value) {
    return null;
  }
  return value.match(/^[0-9]+(\.[0-9]*){0,1}$/g) ? null : { pattern: true };
};
