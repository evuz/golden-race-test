export const resultText = (profit) => {
  return profit > 0 ? winText(profit) : loseText(profit);
}
export const winText = (profit) => `You win ${profit}€!`;
export const loseText = (profit) => `You lose ${Math.abs(profit)}€`;
