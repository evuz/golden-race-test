import { TestBed, ComponentFixture, async } from '@angular/core/testing';
import { AppComponent } from './app.component';

import { ModalService } from './components/modal/modal.service';
import { GameService } from './services/game/game.service';
import { UserService } from './services/user/user.service';

import { ModalModule } from './components/modal/modal.module';
import { BallSelectorModule } from './containers/ball-selector/ball-selector.module';
import { BetSlipModule } from './containers/bet-slip/bet-slip.module';
import { By } from '@angular/platform-browser';

describe('AppComponent', () => {
  let component: AppComponent;
  let fixture: ComponentFixture<AppComponent>;
  let openModalSpy;

  beforeEach(async(() => {
    localStorage.removeItem('PLAYER_AMOUNT');
    TestBed.configureTestingModule({
      declarations: [AppComponent],
      imports: [ModalModule, BallSelectorModule, BetSlipModule],
      providers: [ModalService, GameService, UserService],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppComponent);
    const modalSrv = TestBed.get(ModalService);
    openModalSpy = spyOn(modalSrv, 'openModal');
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create the app', () => {
    expect(component).toBeTruthy();
  });

  it('set welcome credit', () => {
    expect(openModalSpy).toHaveBeenCalled();
  });

  describe('Play', () => {
    let userSrv: UserService;
    let gameSrv: GameService;
    beforeEach(() => {
      userSrv = TestBed.get(UserService);
      gameSrv = TestBed.get(GameService);

      userSrv.setAmount(400);
    });

    it('stake greater than amount', () => {
      gameSrv.setStake(500);
      const el = fixture.debugElement.query(By.css('gr-bet-slip'));
      el.triggerEventHandler('onPlay', null);
      expect(openModalSpy).toHaveBeenCalledTimes(2);
    });
  });
});
