import { Injectable } from '@angular/core';
import { ReplaySubject } from 'rxjs';

const AMOUNT_STORAGE_KEY = 'PLAYER_AMOUNT';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  private _amount: number;
  amount = new ReplaySubject<number>(1);

  constructor() {
    const amount = localStorage.getItem(AMOUNT_STORAGE_KEY);
    this._amount = amount && +amount;
    this.amount.next(this._amount);
  }

  updateAmount(profit) {
    this.setAmount(this._amount + profit);
    return this._amount;
  }

  setAmount(value) {
    localStorage.setItem(AMOUNT_STORAGE_KEY, value);
    this._amount = value;
    this.amount.next(value);
  }
}
