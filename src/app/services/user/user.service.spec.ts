import { TestBed } from '@angular/core/testing';

import { UserService } from './user.service';

const INITIAL_VALUE = '300';

describe('UserService', () => {
  let userSrv: UserService;

  beforeEach(() => {
    localStorage.setItem('PLAYER_AMOUNT', INITIAL_VALUE);
    TestBed.configureTestingModule({});
  });

  beforeEach(() => {
    userSrv = TestBed.get(UserService);
  });

  it('should be created', () => {
    expect(userSrv).toBeTruthy();
  });

  it('initial amount', () => {
    let amount;
    userSrv.amount.subscribe(a => {
      amount = a;
    });
    expect(amount).toEqual(+INITIAL_VALUE);
  });

  it('set amount', () => {
    let amount;
    userSrv.amount.subscribe(a => {
      amount = a;
    });
    userSrv.setAmount(500);
    expect(amount).toEqual(500);
  });

  it('update amount', () => {
    let amount;
    userSrv.amount.subscribe(a => {
      amount = a;
    });
    userSrv.updateAmount(100);
    expect(amount).toEqual(400);
  });
});
