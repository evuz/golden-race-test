import { Injectable } from '@angular/core';
import { ReplaySubject, Observable, zip as observableZip } from 'rxjs';
import { map, take } from 'rxjs/operators';

import { PlayResult } from './models/play-result.model';

const N = 10;
const WAGER = 1.5;

@Injectable({
  providedIn: 'root',
})
export class GameService {
  balls: number[];
  ballsWithoutSelect = new ReplaySubject<number[]>(1);
  ballSelected = new ReplaySubject<number>(1);
  history = new ReplaySubject<number>(10);
  stake = new ReplaySubject<number>(1);

  constructor() {
    this.balls = Array.from({ length: N }).map((_, i) => i + 1);
    this.ballsWithoutSelect.next(this.balls);
  }

  selectBall(ballSelected: number) {
    const ballsWithoutSelect = this.balls.filter(ball => ball !== ballSelected);
    this.ballSelected.next(ballSelected);
    this.ballsWithoutSelect.next(ballsWithoutSelect);
  }

  setStake(value: number) {
    this.stake.next(value);
  }

  play(): Observable<PlayResult> {
    return observableZip(this.ballSelected, this.stake).pipe(
      take(1),
      map(([ballSelected, stake]) => {
        const random = this.generateRandom();
        this.history.next(random);
        return { profit: random === ballSelected ? stake * WAGER : -stake, winner: random };
      }),
    );
  }

  private generateRandom() {
    return Math.floor(Math.random() * N) + 1;
  }
}
