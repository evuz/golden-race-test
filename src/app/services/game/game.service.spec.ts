import { TestBed } from '@angular/core/testing';

import { GameService } from './game.service';

describe('GameService', () => {
  let gameSrv: GameService;

  beforeEach(() => TestBed.configureTestingModule({}));

  beforeEach(() => {
    gameSrv = TestBed.get(GameService);
  });

  it('should be created', () => {
    expect(gameSrv).toBeTruthy();
  });

  it('select ball', () => {
    let selected: number;
    let withoutSelected: number[];
    gameSrv.ballSelected.subscribe(n => {
      selected = n;
    });
    gameSrv.ballsWithoutSelect.subscribe(arr => {
      withoutSelected = arr;
    });
    gameSrv.selectBall(3);
    expect(selected).toEqual(3);
    expect(withoutSelected).toEqual([1, 2, 4, 5, 6, 7, 8, 9, 10]);
  });

  it('stake', () => {
    let stake: number;
    gameSrv.stake.subscribe(n => {
      stake = n;
    });
    gameSrv.setStake(10);
    expect(stake).toEqual(10);
  });

  it('random', () => {
    const random = (<any>gameSrv).generateRandom();
    expect(random).not.toBeNaN();
  });

  it('play win', () => {
    let profit: number;
    let winner: number;
    const randomSpy = spyOn(<any>gameSrv, 'generateRandom');
    randomSpy.and.returnValue(3);
    gameSrv.selectBall(3);
    gameSrv.setStake(10);
    gameSrv.play().subscribe(r => {
      profit = r.profit;
      winner = r.winner;
    });
    expect(winner).toEqual(3);
    expect(profit).toEqual(15);
  });

  it('play lose', () => {
    let profit: number;
    let winner: number;
    const randomSpy = spyOn(<any>gameSrv, 'generateRandom');
    randomSpy.and.returnValue(5);
    gameSrv.selectBall(3);
    gameSrv.setStake(10);
    gameSrv.play().subscribe(r => {
      profit = r.profit;
      winner = r.winner;
    });
    expect(winner).toEqual(5);
    expect(profit).toEqual(-10);
  });
});
