import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BallComponent } from './ball.component';

@NgModule({
  declarations: [BallComponent],
  imports: [CommonModule],
  exports: [BallComponent],
})
export class BallModule {}
