import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

import { BallComponent } from './ball.component';

describe('BallComponent', () => {
  let component: BallComponent;
  let fixture: ComponentFixture<BallComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BallComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BallComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('Correct number', () => {
    component.number = 3;
    fixture.detectChanges();
    expect(fixture.nativeElement.querySelector('span').textContent).toEqual('3');
  });

  it('Style color', () => {
    component.number = 3;
    component.bgColor = 'black';
    component.color = 'white';
    fixture.detectChanges();
    const el = fixture.nativeElement.querySelector('.ball');
    expect(el.style.backgroundColor).toEqual('black');
    expect(el.style.color).toEqual('white');
  });

  it('Style size', () => {
    component.size = 48;
    fixture.detectChanges();
    const el = fixture.nativeElement.querySelector('.ball');
    expect(el.style.width).toEqual('48px');
    expect(el.style.height).toEqual('48px');
  });

  it('Check click', () => {
    let value: number;
    const el = fixture.debugElement.query(By.css('.ball'));
    component.number = 5;
    component.onClick.subscribe(n => {
      value = n;
    })
    el.triggerEventHandler('click', null);
    fixture.detectChanges();
    expect(value).toEqual(5);
  });
});
