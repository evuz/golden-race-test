import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'gr-ball, [gr-ball]',
  templateUrl: './ball.component.html',
  styleUrls: ['./ball.component.scss'],
})
export class BallComponent {
  @Input() number: number;
  @Input() bgColor = 'white';
  @Input() color = 'black';
  @Input() size: number;
  @Output() onClick = new EventEmitter();

  get style() {
    return {
      backgroundColor: this.bgColor,
      color: this.color,
      width: this.size && `${this.size}px`,
      height: this.size && `${this.size}px`,
    };
  }

  handleClick() {
    if (this.number) {
      this.onClick.emit(this.number);
    }
  }
}
