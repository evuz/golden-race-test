import { TestBed } from '@angular/core/testing';

import { ModalService } from './modal.service';

const msgStub = {
  msg: 'Test message',
  ballNumber: 3,
  btnText: 'Test button',
};

describe('ModalService', () => {
  let modalSrv: ModalService;

  beforeEach(() => TestBed.configureTestingModule({ providers: [ModalService]}));

  beforeEach(() => {
    modalSrv = TestBed.get(ModalService);
  });

  it('should be created', () => {
    expect(modalSrv).toBeTruthy();
  });

  it('open', () => {
    let isOpen;
    modalSrv.open.subscribe(msg => isOpen = msg);
    modalSrv.openModal(msgStub);
    expect(isOpen).toEqual(msgStub);
  });

  it('close', () => {
    let isClose;
    modalSrv.close.subscribe(() => isClose = true);
    modalSrv.closeModal();
    expect(isClose).toBeTruthy();
  });

  it('close after open', () => {
    let isClose;
    let msgSended;
    modalSrv.open.subscribe(msg => msgSended = msg);
    modalSrv.openModal(msgStub).subscribe(() => isClose = true);
    modalSrv.closeModal();
    expect(msgSended).toEqual(msgStub);
    expect(isClose).toBeTruthy();
  });
});
