export interface Message {
  msg: string;
  ballNumber?: number;
  btnText?: string;
}
