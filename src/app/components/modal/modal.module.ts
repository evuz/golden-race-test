import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BallModule } from '../ball/ball.module';
import { ButtonModule } from '../button/button.module';

import { ModalComponent } from './modal.component';

@NgModule({
  declarations: [ModalComponent],
  imports: [CommonModule, BallModule, ButtonModule],
  exports: [ModalComponent],
})
export class ModalModule {}
