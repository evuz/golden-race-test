import { Component, OnInit } from '@angular/core';

import { ModalService } from './modal.service';
import { Message } from './models/message.model';

import { colors } from 'src/app/utils/colors';

const BTN_TEXT_DEFAULT = 'Accept';

@Component({
  selector: 'gr-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss'],
})
export class ModalComponent implements OnInit {
  showModal: boolean;
  msg: string;
  ballNumber: number;
  btnText = BTN_TEXT_DEFAULT;
  colors = colors;

  constructor(private modalSrv: ModalService) {}

  ngOnInit() {
    this.modalSrv.close.subscribe(() => {
      this.setMessage();
      this.showModal = false;
    });
    this.modalSrv.open.subscribe(msg => {
      this.setMessage(msg);
      this.showModal = true;
    });
  }

  setMessage({ msg, ballNumber, btnText }: Message = <any>{}) {
    this.msg = msg;
    this.ballNumber = ballNumber;
    this.btnText = btnText || BTN_TEXT_DEFAULT;
  }

  close() {
    this.modalSrv.closeModal();
  }
}
