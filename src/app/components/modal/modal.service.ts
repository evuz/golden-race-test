import { Injectable } from '@angular/core';
import { Observable, ReplaySubject } from 'rxjs';
import { take } from 'rxjs/operators';

import { Message } from './models/message.model';

@Injectable()
export class ModalService {
  open = new ReplaySubject<Message>(1);
  close = new ReplaySubject<undefined>(1);

  constructor() { }

  openModal(msg: Message): Observable<undefined> {
    this.open.next(msg);
    return this.close.pipe(take(1));
  }

  closeModal() {
    this.close.next();
  }
}
