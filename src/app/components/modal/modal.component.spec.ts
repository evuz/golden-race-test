import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

import { ModalComponent } from './modal.component';
import { ButtonComponent } from '../button/button.component';
import { BallComponent } from '../ball/ball.component';

import { ModalService } from './modal.service';

const msgStub = {
  msg: 'Test message',
  ballNumber: 3,
  btnText: 'Test button',
};

describe('ModalComponent', () => {
  let component: ModalComponent;
  let fixture: ComponentFixture<ModalComponent>;
  let modalSrv: ModalService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ModalComponent, ButtonComponent, BallComponent],
      providers: [ModalService],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalComponent);
    modalSrv = TestBed.get(ModalService);
    component = fixture.componentInstance;
    modalSrv.openModal(msgStub);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should be open', () => {
    expect(component.showModal).toBeTruthy();
  });

  it('message', () => {
    const el = fixture.nativeElement.querySelector('.text');
    expect(el.textContent).toEqual(msgStub.msg);
  });

  it('ball', () => {
    expect(component.ballNumber).toEqual(msgStub.ballNumber);
  });

  it('text button', () => {
    expect(component.btnText).toEqual(msgStub.btnText);
  });

  it('close', () => {
    const el = fixture.debugElement.query(By.css('button'));
    el.triggerEventHandler('click', null);
    fixture.detectChanges();
    expect(component.showModal).toBeFalsy();
  });
});
