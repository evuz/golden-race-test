import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BallsComponent } from './balls.component';
import { BallModule } from '../ball/ball.module';

@NgModule({
  declarations: [BallsComponent],
  imports: [CommonModule, BallModule],
  exports: [BallsComponent],
})
export class BallsModule {}
