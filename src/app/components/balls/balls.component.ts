import { Component, Input, Output, EventEmitter } from '@angular/core';
import { BallColor } from './ball-color.model';

@Component({
  selector: 'gr-balls',
  templateUrl: './balls.component.html',
  styleUrls: ['./balls.component.scss'],
})
export class BallsComponent {
  @Input() balls: number[];
  @Input() colors: { [e: number]: BallColor } = [];
  @Input() clickable: boolean;
  @Output() onClick = new EventEmitter();

  constructor() {}

  handleClick(event) {
    this.onClick.emit(event);
  }
}
