import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

import { BallsComponent } from './balls.component';
import { BallComponent } from '../ball/ball.component';

const balls = [1, 2, 3, 4, 5];
const colors = { 1: { background: 'red', font: 'blue' } };
describe('BallsComponent', () => {
  let component: BallsComponent;
  let fixture: ComponentFixture<BallsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BallsComponent, BallComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BallsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('correct number balls', () => {
    component.balls = balls;
    fixture.detectChanges();
    const elements = fixture.nativeElement.getElementsByTagName('gr-ball');
    expect(elements.length).toEqual(5);
  });

  it('correct number balls', () => {
    component.balls = balls;
    component.colors = colors;
    fixture.detectChanges();
    const el = fixture.nativeElement.querySelector('.ball');
    expect(el.style.backgroundColor).toEqual('red');
    expect(el.style.color).toEqual('blue');
  });

  it('Check click', () => {
    let value: number;
    component.balls = balls;
    component.colors = colors;
    fixture.detectChanges();
    const el = fixture.debugElement.queryAll(By.css('.ball'))[3];
    component.onClick.subscribe(n => {
      value = n;
    });
    el.triggerEventHandler('click', null);
    fixture.detectChanges();
    expect(value).toEqual(4);
  });

  it('Check clickable input', () => {
    component.clickable = true;
    component.balls = balls;
    fixture.detectChanges();
    const elements = fixture.debugElement.queryAll(By.css('gr-ball'));
    expect(elements.filter(el => el.classes.clickable).length).toEqual(5);
  });
});
