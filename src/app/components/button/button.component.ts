import { Component, Input, HostBinding } from '@angular/core';

@Component({
  selector: 'gr-button, [gr-button]',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss']
})
export class ButtonComponent {
  @Input() full: boolean;
  @HostBinding('class.full') get fullClass () {
    return this.full;
  }
}
