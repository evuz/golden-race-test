import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InputComponent } from './input.component';

describe('InputComponent', () => {
  let component: InputComponent;
  let fixture: ComponentFixture<InputComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [InputComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('change value', () => {
    const input = fixture.nativeElement.querySelector('input');
    input.value = 'Test value';
    input.dispatchEvent(new Event('input'));
    fixture.detectChanges();
    expect(component.value).toEqual('Test value');
  });

  it('check label', () => {
    component.label = 'Label';
    fixture.detectChanges();
    const el = fixture.nativeElement.querySelector('label');
    expect(el.textContent).toEqual('Label');
  });

  it('check error', () => {
    component.error = 'Show error';
    fixture.detectChanges();
    const el = fixture.nativeElement.querySelector('.error');
    expect(el.textContent).toEqual('Show error');
  });

  it('check no error', () => {
    component.error = undefined;
    const el = fixture.nativeElement.querySelector('.error');
    expect(el).toBeNull();
  });

  it('check placeholder', () => {
    component.placeholder = 'Placeholder';
    fixture.detectChanges();
    const el = fixture.nativeElement.querySelector('input');
    expect(el.placeholder).toEqual('Placeholder');
  });

  it('check type', () => {
    component.type = 'tel';
    fixture.detectChanges();
    const el = fixture.nativeElement.querySelector('input');
    expect(el.type).toEqual('tel');
  });
});
