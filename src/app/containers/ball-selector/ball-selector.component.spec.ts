import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

import { BallSelectorComponent } from './ball-selector.component';
import { ButtonComponent } from 'src/app/components/button/button.component';
import { BallComponent } from 'src/app/components/ball/ball.component';
import { BallsComponent } from 'src/app/components/balls/balls.component';
import { GameService } from 'src/app/services/game/game.service';

describe('BallSelectorComponent', () => {
  let component: BallSelectorComponent;
  let fixture: ComponentFixture<BallSelectorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BallSelectorComponent, ButtonComponent, BallComponent, BallsComponent],
      providers: [GameService],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BallSelectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('select number', () => {
    const el = fixture.debugElement.query(By.css('gr-balls'));
    el.triggerEventHandler('onClick', 3);
    fixture.detectChanges();
    expect(component.ballSelected).toEqual(3);
  })

  it('clear selection', () => {
    component.selectNumber(3);
    fixture.detectChanges();
    const el = fixture.debugElement.query(By.css('button'));
    el.triggerEventHandler('click', null);
    component.clearSelection();
    fixture.detectChanges();
    expect(component.ballSelected).toEqual(undefined);
  })
});
