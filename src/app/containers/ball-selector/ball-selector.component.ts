import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable, Subject } from 'rxjs';

import { GameService } from 'src/app/services/game/game.service';

import { colors } from '../../utils/colors';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'gr-ball-selector',
  templateUrl: './ball-selector.component.html',
  styleUrls: ['./ball-selector.component.scss'],
})
export class BallSelectorComponent implements OnInit, OnDestroy {
  destroySubject = new Subject();
  balls: Observable<number[]>;
  ballSelected: number;
  colors = colors;

  constructor(private gameSrv: GameService) {}

  ngOnInit() {
    this.balls = this.gameSrv.ballsWithoutSelect;
    this.gameSrv.ballSelected.pipe(takeUntil(this.destroySubject)).subscribe(number => (this.ballSelected = number));
  }

  ngOnDestroy() {
    this.destroySubject.next();
  }

  clearSelection() {
    this.gameSrv.selectBall(undefined);
  }

  selectNumber(number) {
    this.gameSrv.selectBall(number);
  }
}
