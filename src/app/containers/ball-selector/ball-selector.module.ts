import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BallsModule } from 'src/app/components/balls/balls.module';

import { BallSelectorComponent } from './ball-selector.component';
import { ButtonModule } from 'src/app/components/button/button.module';
import { BallModule } from 'src/app/components/ball/ball.module';

@NgModule({
  declarations: [BallSelectorComponent],
  imports: [CommonModule, BallsModule, BallModule, ButtonModule],
  exports: [BallSelectorComponent],
})
export class BallSelectorModule {}
