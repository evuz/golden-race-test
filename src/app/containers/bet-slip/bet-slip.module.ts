import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { BallsModule } from 'src/app/components/balls/balls.module';
import { ButtonModule } from 'src/app/components/button/button.module';
import { BetSlipComponent } from './bet-slip.component';
import { InputModule } from 'src/app/components/input/input.module';

@NgModule({
  declarations: [BetSlipComponent],
  imports: [CommonModule, BallsModule, ButtonModule, InputModule, ReactiveFormsModule],
  exports: [BetSlipComponent],
})
export class BetSlipModule {}
