import { Component, OnInit, OnDestroy, Output, EventEmitter } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { GameService } from 'src/app/services/game/game.service';
import { UserService } from 'src/app/services/user/user.service';

import { colors } from 'src/app/utils/colors';
import { patternValidator } from 'src/app/utils/patternValidator';

@Component({
  selector: 'gr-bet-slip',
  templateUrl: './bet-slip.component.html',
  styleUrls: ['./bet-slip.component.scss'],
})
export class BetSlipComponent implements OnInit, OnDestroy {
  @Output() onPlay = new EventEmitter();
  destroySubject = new Subject();
  formGroup: FormGroup;
  history: number[] = [];
  isBallSelected: boolean;
  colors = colors;
  stake: number;
  amount: number;

  get buttonEnable() {
    return this.stake && this.formGroup.valid && this.isBallSelected;
  }

  get inputError() {
    return this.formGroup.touched && this.formGroup.invalid && 'You must enter a number greater than 5 and amount';
  }

  constructor(private gameSrv: GameService, private userSrv: UserService) {
    this.formGroup = new FormGroup({
      stake: new FormControl(null, [Validators.required, Validators.min(5), patternValidator]),
    });
  }

  ngOnInit() {
    this.gameSrv.ballSelected.pipe(takeUntil(this.destroySubject)).subscribe(val => (this.isBallSelected = !!val));
    this.gameSrv.stake.pipe(takeUntil(this.destroySubject)).subscribe(val => (this.stake = val));
    this.userSrv.amount.pipe(takeUntil(this.destroySubject)).subscribe(val => (this.amount = val));
    this.gameSrv.history
      .pipe(takeUntil(this.destroySubject))
      .subscribe(val => (this.history = [...this.history, val].slice(-10)));
  }

  ngOnDestroy() {
    this.destroySubject.next();
    this.destroySubject.complete();
  }

  submit() {
    const {
      value: { stake },
    } = this.formGroup;
    if (this.formGroup.valid) {
      this.gameSrv.setStake(stake);
    }
  }

  play() {
    if (this.buttonEnable) {
      this.onPlay.emit();
    }
  }
}
